#include <SFML/Graphics.hpp>
#include "maploader.h"
#include "playermovements.h"

// V�rifier tous les TODO


int main()
{
    sf::RenderWindow window(sf::VideoMode(1024, 768), "Big Cool Bomb Party");
    window.setVerticalSyncEnabled(true);
    window.setFramerateLimit(60);

    sf::RenderTexture target;
    if (!target.create(window.getSize().x, window.getSize().y))
        return -1;

    //Texture player1
    sf::Texture imageBack1;
    sf::Texture imageBack2;
    sf::Texture imageBack3;
    sf::Texture imageForward1;
    sf::Texture imageForward2;
    sf::Texture imageForward3;
    sf::Texture imageLeft1;
    sf::Texture imageLeft2;
    sf::Texture imageLeft3;
    sf::Texture imageLeft4;
    sf::Texture imageRight1;
    sf::Texture imageRight2;
    sf::Texture imageRight3;
    sf::Texture imageRight4;

    sf::Texture mapTexture;

    //if (!mapTexture.loadFromFile("Background.png")) 
       // return -1;

    if (!imageBack1.loadFromFile("Back1.png"))
        printf("PB de chargement de l'image !\n");
    if (!imageBack2.loadFromFile("Back2.png"))
        printf("PB de chargement de l'image !\n");
    if (!imageBack3.loadFromFile("Back3.png"))
        printf("PB de chargement de l'image !\n");
    if (!imageForward1.loadFromFile("Forward1.png"))
        printf("PB de chargement de l'image !\n");
    if (!imageForward2.loadFromFile("Forward2.png"))
        printf("PB de chargement de l'image !\n");
    if (!imageForward3.loadFromFile("Forward3.png"))
        printf("PB de chargement de l'image !\n");
    if (!imageLeft1.loadFromFile("Left1.png"))
        printf("PB de chargement de l'image !\n");
    if (!imageLeft2.loadFromFile("Left2.png"))
        printf("PB de chargement de l'image !\n");
    if (!imageLeft3.loadFromFile("Left3.png"))
        printf("PB de chargement de l'image !\n");
    if (!imageLeft4.loadFromFile("Left4.png"))
        printf("PB de chargement de l'image !\n");
    if (!imageRight1.loadFromFile("Right1.png"))
        printf("PB de chargement de l'image !\n");
    if (!imageRight2.loadFromFile("Right2.png"))
        printf("PB de chargement de l'image !\n");
    if (!imageRight3.loadFromFile("Right3.png"))
        printf("PB de chargement de l'image !\n");
    if (!imageRight4.loadFromFile("Right4.png"))
        printf("PB de chargement de l'image !\n");

    sf::Vector2f player1(10, 10);
    sf::Vector2f player2(590, 10);


    sf::Sprite map(mapTexture);
    sf::Sprite paddle1;
    sf::Sprite paddle2;
    paddle1.setTexture(imageBack1);
    paddle2.setTexture(imageBack1);
// Charger la map
    mapLoader(target);
   /* target.clear(sf::Color::White);
    target.draw(, sf::BlendAlpha);
    target.display();*/
    //target.display();
    sf::Sprite map2(target.getTexture()); // stocker la texture cr�� dans le Sprite map2
// Fin Charger la map

    // Initialisation des variables
    //Variables pour les animations des d�placements
    //player1
    int firstForward = 0;
    int firstBack = 0;
    int firstLeft = 0;
    int firstRight = 0;
    //player2
    int firstForward2 = 0;
    int firstBack2 = 0;
    int firstLeft2 = 0;
    int firstRight2 = 0;

    //time
    sf::Clock clock;
    sf::Clock clockUp;
    sf::Clock clockDown;
    sf::Clock clockRight;
    sf::Clock clockLeft;
    sf::Time time;
    sf::Time timeUp;
    sf::Time timeDown;
    sf::Time timeRight;
    sf::Time timeLeft;
    //player1
    float dt = 0;
    float dtUp = 0;
    float dtDown = 0;
    float dtRight = 0;
    float dtLeft = 0;
    //player2
    //float dt2 = 0;
    float dtUp2 = 0;
    float dtDown2 = 0;
    float dtRight2 = 0;
    float dtLeft2 = 0;

    int speed = 200;
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }


        // Debut mouvement joueurs

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) && !(sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) && !(sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) && !(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))) // Fleche gauche appuyee
        {
            move_left(player1, paddle1, imageLeft1, imageLeft2, imageLeft3, clockLeft, speed, dt, dtLeft, firstLeft);
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && !(sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) && !(sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) && !(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))) // Fleche droite appuyee
        {
            move_right(player1, paddle1, imageRight1,imageRight2,imageRight3,clockRight,speed,dt,dtRight,firstRight);
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && !(sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) && !(sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) && !(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))) // Fleche du haut appuyee
        {        
            move_up(player1,paddle1,imageForward1,imageForward2,imageForward3,clockUp,speed,dt,dtUp,firstForward);
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down) && !(sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) && !(sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) && !(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))) // Fleche du bas appuyee
        {
            move_down(player1, paddle1, imageBack1, imageBack2, imageBack3, clockDown, speed, dt, dtDown, firstBack);
        }

        //player 2

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Q) && !(sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Z)) && !(sf::Keyboard::isKeyPressed(sf::Keyboard::Key::D)) && !(sf::Keyboard::isKeyPressed(sf::Keyboard::Key::S))) // Fleche gauche appuyee
        {
            move_left(player2, paddle2, imageLeft1, imageLeft2, imageLeft3, clockLeft, speed, dt, dtLeft2, firstLeft2);
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::D) && !(sf::Keyboard::isKeyPressed(sf::Keyboard::Key::S)) && !(sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Z)) && !(sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Q))) // Fleche droite appuyee
        {
            move_right(player2, paddle2, imageRight1, imageRight2, imageRight3, clockRight, speed, dt, dtRight2, firstRight2);
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Z) && !(sf::Keyboard::isKeyPressed(sf::Keyboard::Key::S)) && !(sf::Keyboard::isKeyPressed(sf::Keyboard::Key::D)) && !(sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Q))) // Fleche du haut appuyee
        {
            move_up(player2, paddle2, imageForward1, imageForward2,imageForward3, clockUp, speed, dt, dtUp2, firstForward2);
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::S) && !(sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Z)) && !(sf::Keyboard::isKeyPressed(sf::Keyboard::Key::D)) && !(sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Q))) // Fleche du bas appuyee
        {
            move_down(player2, paddle2, imageBack1, imageBack2, imageBack3, clockDown, speed, dt, dtDown2, firstBack2);
        }

        // Fin deplacement joueurs
        time = clock.restart();
        dt = time.asSeconds();
        // Update players movement
        paddle1.setPosition(player1.x, player1.y);
        paddle2.setPosition(player2.x, player2.y);


        window.clear();
        window.draw(map2);
        window.draw(paddle1);
        window.draw(paddle2);
        window.display();
    }
}
