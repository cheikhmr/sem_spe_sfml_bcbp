#include <SFML/Graphics.hpp>
#include "maploader.h" 
#define NBRTILE0 7
#define NBRTILE1 9
#define NBRTILE2 1
#define NBRTILE3 5
#define NBRTILE4 1
#define SIZETILES 30   // en pixel
void mapLoader(sf::RenderTexture &target)
{
    const int level[] =
    {
        2, 0, 1, 1, 0, 1, 0, 1, 1, 0, 0,
        0, 3, 1, 3, 1, 3, 1, 3, 1, 3, 0,      
    };
    sf::Texture textureTile0;
    sf::Texture textureTile1;
    sf::Texture textureTile2;
    sf::Texture textureTile3;
   // sf::Texture textureTile4;
    if (!textureTile0.loadFromFile("Tile0.png"))
        printf("PB de chargement de l'image !\n");
    if (!textureTile1.loadFromFile("Tile1.png"))
        printf("PB de chargement de l'image !\n");
    if (!textureTile2.loadFromFile("Tile2.png"))
        printf("PB de chargement de l'image !\n");
    if (!textureTile3.loadFromFile("Tile3.png"))
        printf("PB de chargement de l'image !\n");
    //if (!textureTile3.loadFromFile("Tile4.png"))
    //    printf("PB de chargement de l'image !\n");
    sf::Sprite tile0[NBRTILE0];
    sf::Sprite tile1[NBRTILE1] ;
    sf::Sprite tile2[NBRTILE2] ;
    sf::Sprite tile3[NBRTILE3] ;
   // sf::Sprite tile4[NBRTILE4] = {};
    int i;
    int posX;
    int posY;
    int tile0Round = 0, tile1Round = 0, tile2Round = 0, tile3Round = 0;// tile4Round = 0; // permet de connaitre la bonne case du tableau
    for (i = 0; i <= 21; i++) {
        posY = i / 12;
        posX = i % 12;
        switch (level[i])
        {
        case 0: 
            tile0[tile0Round].setTexture(textureTile0);
            tile0[tile0Round].setPosition(posX * SIZETILES, posY * SIZETILES);
            tile0Round++;
            break;
        case 1: 
            tile1[tile1Round].setTexture(textureTile1);
            tile1[tile1Round].setPosition(posX * SIZETILES, posY * SIZETILES);
            tile1Round++;
            break;
        case 2: 
            tile2[tile2Round].setTexture(textureTile2);
            tile2[tile2Round].setPosition(posX * SIZETILES, posY * SIZETILES);
            tile2Round++;
            break;
        case 3:
            tile3[tile3Round].setTexture(textureTile3);
            tile3[tile3Round].setPosition(posX * SIZETILES, posY * SIZETILES);
            tile3Round++;
            break;
        /*case 4:
            tile4[tile4Round].setTexture(textureTile4);
            tile4[tile4Round].setPosition(posX * SIZETILES, posY * SIZETILES);
            tile4Round++;
            break;*/
        default:
            break;
        }
    }
    target.clear(sf::Color::White);
    for(i=0;i<NBRTILE0; i++)
    target.draw(tile0[i], sf::BlendAlpha);
    for (i = 0; i < NBRTILE1; i++)
        target.draw(tile1[i], sf::BlendAlpha);
    for (i = 0; i < NBRTILE2; i++)
        target.draw(tile2[i], sf::BlendAlpha);
    for (i = 0; i < NBRTILE3; i++)
        target.draw(tile3[i], sf::BlendAlpha);
   // for (i = 0; i++; i < NBRTILE4)
     //   target.draw(tile4[i]);
    target.display();
    
}

